class PGPSupport < ActiveRecord::Migration[5.2]
  def up
    # return if it's a new setup
    # return unless Setting.exists?(name: 'system_init_done')

    Setting.create_if_not_exists(
      title: 'PGP integration',
      name: 'pgp_integration',
      area: 'Integration::Switch',
      description: 'Defines if PGP encryption is enabled or not.',
      options: {
        form: [
          {
            display: '',
            null: true,
            name: 'pgp_integration',
            tag: 'boolean',
            options: {
              true => 'yes',
              false => 'no'
            }
          }
        ]
      },
      state: false,
      preferences: {
        prio: 1,
        authentication: true,
        permission: ['admin.integration']
      },
      frontend: true
    )
    Setting.create_if_not_exists(
      title: 'PGP config',
      name: 'pgp_config',
      area: 'Integration::PGP',
      description: 'Defines the PGP config.',
      options: {},
      state: {},
      preferences: {
        prio: 2,
        permission: ['admin.integration']
      },
      frontend: true
    )
    Setting.create_if_not_exists(
      title: 'Defines postmaster filter.',
      name: '0016_postmaster_filter_smime',
      area: 'Postmaster::PreFilter',
      description: 'Defines postmaster filter to handle secure mailing.',
      options: {},
      state: 'Channel::Filter::SecureMailing',
      frontend: false
    )

    create_table :pgp_keypairs do |t|
      t.string :fingerprint, limit: 250, null: false
      t.binary :public_key,                limit: 10.megabytes, null: false
      t.binary :private_key,        limit: 10.megabytes, null: true
      t.string :private_key_secret, limit: 500, null: true
      t.timestamps limit: 3, null: false
    end
    add_index :pgp_keypairs, [:fingerprint], unique: true
  end
end
