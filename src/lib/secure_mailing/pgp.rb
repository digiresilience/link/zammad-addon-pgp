# Copyright (C) 2012-2022 Zammad Foundation, https://zammad-foundation.org/

class SecureMailing::PGP < SecureMailing::Backend
  def self.active?
    Setting.get('pgp_integration')
  end
end
